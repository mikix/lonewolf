# Lone Wolf

You are the sole survivor of a devastating attack on the monastery where you were learning the skills of the Kai Lords. You swear vengeance on the Darklords for the massacre of the Kai warriors, and with a sudden flash of insight you know what you must do. You must set off on a perilous journey to the capital city to warn the King of the terrible threat that faces his people: For you are now the last of the Kai. You are now Lone Wolf.

Lone Wolf is a role-playing book series from the 80s. This app lets you play the old adventures on modern devices.

# Installing

Simply run `snap install lonewolf`
